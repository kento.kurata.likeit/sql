select
i.item_id, i.item_name, i.item_price, j.category_name
from
item i
inner join
item_category j
on
i.`category_id` = j.`category_id`;
