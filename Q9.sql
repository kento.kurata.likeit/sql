select
j.category_name, sum(i.item_price) as total_price
from
item_category j
inner join
item i
on
i.category_id = j.category_id
group by
category_name
order by
total_price desc;	
